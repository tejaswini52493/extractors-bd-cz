# -------------------------------------------------------------------------------------------------------------------------
# This extracts floodplain surfaces from given clipped DEM and single stream shapefile.
# This uses TerEx script provided in the http://cnr.usu.edu/htm/facstaff/belmont-hydrology-and-fine-sediment-lab/belmont-lab-resources
# ---------------------------------------------------------------------------------------------------------------------------
from __future__ import division
# Import arcpy module
import arcpy
from arcpy import env
from config import *
import pyclowder.extractors as extractors

import os,os.path
import subprocess
import shutil
import sys
import requests
import logging
import re
import json
import tempfile
from terex_step1_dec12 import *
from multiprocessing import Process
import time
import xmltodict

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL 
    global logger   
    
    #set logging
    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger('floodplain')
    logger.setLevel(logging.DEBUG)
    #set up
    extractors.setup(extractorName=extractorName, messageType=messageType, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL, sslVerify=sslVerify)
    
    #registration
    extractors.register_extractor(registrationEndpoints)
        
    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=process_file, 
        rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

# ----------------------------------------------------------------------
#zip the shapefile 
def zipshp(name, dir_name,outname):
    subprocess.check_call(['7z', 'a','-tzip', dir_name+"\\"+outname+".zip", dir_name+"\\"+name+'*'], shell=False)
    return name  
#------------------------------------------------------------------------

#unzip the input file that consists of a tif and a zipped shapefile
def unzip(arcgis_tmpDir, inzip):
    tmpDir=arcgis_tmpDir+"\\input_dir"
    os.makedirs(tmpDir)
    subprocess.check_call(['7z', 'x', '-o%s' % tmpDir, inzip], shell=False)
    files = [os.path.join(tmpDir,f) for f in os.listdir(tmpDir) ]
    shp_dir_name = ""
    tif_file_name = ""
    for f in files:
        #print "realpath : " + os.path.realpath(f)
        #print os.path.basename(f)
        #if os.path.isdir(f):
        #    shpfiles=[os.path.join(f,fi) for fi in os.listdir(f)]
        #    for fi in shpfiles:
        if f.endswith(".zip"):
            #m = re.split('\.shp.zip',os.path.basename(f))
            m = re.split('\.zip',os.path.basename(f))
            print m[0]
            shp_dir = tmpDir+"\\"+m[0]
            subprocess.check_call(['7z', 'x', '-o%s' %shp_dir, f], shell=False)
            shp_dir_name = m[0]
        if f.endswith(".tif"):
            tif_file_name = os.path.basename(f)        
    return (tmpDir,shp_dir_name,tif_file_name)        

# ----------------------------------------------------------------------
# extract single stream line from the clipped stream line shapefile file
def extract_single_streamline(shapefile,river_name):
    print "Shape file: "+ shapefile 
    p = Process(target=int_stream, args=(shapefile,river_name,))
    p.start()
    p.join()
        
def int_stream(shapefile,river_name):
    print "Shape file 2: "+ shapefile 
    # Create update cursor for feature class
    rows = arcpy.UpdateCursor(shapefile)

    # Iterate through the rows of the attribution table
    for row in rows:
        # Fields from the table can be dynamically accessed from the
        # row object. 
        #if row.getValue("GNIS_NAME")!="Sangamon River":
        if row.getValue("GNIS_NAME")!= river_name:
            rows.deleteRow(row)
    # Delete cursor and row objects to remove locks on the data
    del row
    del rows   
    
# ----------------------------------------------------------------------
# extract flood plain using TerEx 
def extract_floodplain(input_dir,shp_dir_name,tif_file_name, output_dir):
    # Check out any necessary licenses
    arcpy.CheckOutExtension("spatial")
    
   
    # intermediate files' names
    line_dem_tif = output_dir+"\\linedem.tif"
    line_slope_dem_tif = output_dir+"\\line_slope_dem.tif"

    #output directory
    os.makedirs(output_dir+"\\resultDir")
    result_dir = output_dir+"\\resultDir"
    
    #clipped dem input dir path
    clipped_dem = input_dir+"\\"+ tif_file_name
    print 'clipped_dem : '+clipped_dem
    
    for f in os.listdir(input_dir + "\\"+shp_dir_name):
        if f.endswith(".shp"):
            clipped_streamline_shapefile = input_dir + "\\"+shp_dir_name+"\\"+os.path.basename(f)
            break;
    
    river_name = convert_xml_to_dictionary(input_dir+"\\dts-floodplain.xml")
    
    #extract single stream line from the clipped streamline shapefile 
    extract_single_streamline(clipped_streamline_shapefile,river_name)
    
    single_streamline_shp = clipped_streamline_shapefile
    
    
    print 'single_streamline_shp: '+ single_streamline_shp
    
    # Process: Extract by Mask
    arcpy.gp.ExtractByMask_sa(clipped_dem, single_streamline_shp, line_dem_tif)

    # Process: Slope
    arcpy.gp.Slope_sa(line_dem_tif, line_slope_dem_tif, "PERCENT_RISE", "1")

    # Calculate the statistics on the raster data
    arcpy.CalculateStatistics_management(line_slope_dem_tif)
    
    #Get the geoprocessing result object
    resultMEAN = arcpy.GetRasterProperties_management(line_slope_dem_tif, "MEAN")
   
   #Get the elevation standard deviation value from geoprocessing result object
    meanSlope0 = resultMEAN.getOutput(0)
    meanSlope = float(meanSlope0)/100*5*1.2+0.33
    print str(meanSlope)
    
    # Process: TerExStep1
    #terex_process(result_dir, clipped_dem, meanSlope, "100", single_streamline_shp, "1000", "1.2", "5", "45")
    p = Process(target=terex_process, args=(result_dir, clipped_dem, meanSlope, "100", single_streamline_shp, "1000", "1.2", "5", "45",))
    p.start()
    p.join()
      
# -----------------------------------------------------------------------
# create empty dataset in DTS
def create_empty_dataset(dataset_description, host, key):
    global sslVerify
    
    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"

    url = host+'api/datasets/createempty?key=' + key

    r = requests.post(url, headers=headers, data=json.dumps(dataset_description), verify=sslVerify)
    r.raise_for_status()
    datasetid = r.json()['id']
    print 'created an empty dataset : '+ str(datasetid)
    return datasetid
#------------------------------------------------------------------------
# upload file to a dataset in DTS
def upload_file_to_dataset(filepath, datasetid, host, key):
    uploadedfileid = None
    if(not host.endswith("/")):
        host = host+"/"    
    print datasetid 
    url = host+'api/uploadToDataset/'+datasetid+'?key=' + key
    print "url:"+url
    
    f = open(filepath,'rb')
    r = requests.post(url, files={"File":f}, verify=sslVerify)
    r.raise_for_status()
    uploadedfileid = r.json()['id']
    return uploadedfileid   
    
 #--------------------------------------------------------------------------------- 
 # Upload dataset metadata to DTS
def upload_derived_dataset_metadata_jsonld(datasetid, mdata, host, key):

    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"
    url = host+'api/datasets/'+ datasetid +'/metadata.jsonld?key=' + key
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()
#------------------------------------------------------------------------------------
# Upload file metadata to DTS
def upload_derived_file_metadata_jsonld(fileid, mdata, host, key):
    global sslVerify

    headers={'Content-Type': 'application/json'}

    url=host+'api/files/'+ fileid +'/metadata.jsonld?key=' + key
    
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()

#----------------------------------------------------------------------------------------------------------
# Convert input XML file into dictionary
def convert_xml_to_dictionary(input_xml_filename) :
    dict_data = xmltodict.parse(file(input_xml_filename))
    print dict_data['input']['river_name']
    return dict_data['input']['river_name']
#----------------------------------------------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global filename
    filename = ""
    inputfile = parameters['inputfile']
    fileid = parameters['fileid']
    host = parameters['host'] 
    key = parameters['secretKey']
    if(not host.endswith("/")):
        host = host+"/"
    
    #global arcgis_tempDir
    arcgis_tmpDir=""
    try:
        if inputfile is not None and os.path.isfile(inputfile):
            
            m = re.split('\.zip',os.path.basename(inputfile))
            filename = m[0]
            print 'filename:----'+ filename
            #creating temporary workspace
            arcgis_tmpDir = tempfile.mkdtemp()
                      
            #env.workspace = arcgis_tmpDir+"\\output_dir\\resultDir"
            
            print "arcgis_tmpDir : "+arcgis_tmpDir
            (input_dir,shp_dir_name,tif_file_name) = unzip(arcgis_tmpDir,inputfile)
            
            output_dir = arcgis_tmpDir+"\\output_dir"
               
            if shp_dir_name != "" and shp_dir_name.endswith(".shp") and tif_file_name != "":           
               
               
                # the actual program
                extract_floodplain(input_dir,shp_dir_name,tif_file_name,output_dir)
                zipshp("smoothed",output_dir+"\\resultDir", "floodplain")
                
                #generated_files = []
                #generated_dataset =[]
                dataset_des={}
                dataset_des['name']= 'Floodplain'
                dataset_des['description'] = 'created by TerEx using input file with id '+ fileid +' '+ time.strftime('%Y-%m-%dT%H:%M:%S')
                datasetid = create_empty_dataset(dataset_des,host,key)
                print "datasetid: "+ datasetid
                #print output_dir+'\\resultDir\\smoothed.zip'
                dataset_url = host + 'datasets/'+datasetid
                #generated_dataset.append(dataset_url)
                new_fid = upload_file_to_dataset(output_dir+"\\resultDir\\floodplain"+".zip",datasetid,host,key)
                generated_file_url = extractors.get_file_URL(fileid=new_fid, parameters=parameters)
                #generated_files.append(new_url)
                                                                    
                # Context url
                context_url = 'https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld'

                # Store results as metadata
                metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_file': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_file',
                            'generated_dataset': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_dataset'
                        }
                    ],
                    'attachedTo': {
                        'resourceType': 'file', 'id': parameters["fileid"]
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_file': generated_file_url,
                        'generated_dataset': host + 'datasets/' + datasetid
                    }
                }

                #add metadata to original input file
                extractors.upload_file_metadata_jsonld(mdata=metadata, parameters=parameters)

                #build metadata for derived dataset
                derived_dataset_metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                        }
                    ],
                    'attachedTo': {
                        'resourceType': 'dataset', 'id': datasetid
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_from': extractors.get_file_URL(fileid, parameters)
                    }
                }
        
                #build metadata for derived file
                derived_file_metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                        }
                    ],
                    'attachedTo': {
                        'resourceType': 'file', 'id': new_fid
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_from': extractors.get_file_URL(fileid, parameters)
                    }
                }
                           
                #add metadata to derived dataset        
                upload_derived_dataset_metadata_jsonld(datasetid, derived_dataset_metadata, host, key)
        
                #add metadata to derived file        
                upload_derived_file_metadata_jsonld(new_fid, derived_dataset_metadata, host, key)   
                 
                           
    finally:
        if os.path.isdir(arcgis_tmpDir+"\\input_dir"): 
            try:
                print "deleting the input_dir directory:--- "+ arcgis_tmpDir+"\\input_dir"
                shutil.rmtree(arcgis_tmpDir+"\\input_dir")
                
            except:
                pass 
               
        if os.path.isdir(arcgis_tmpDir+"\\output_dir"): 
            try:
                print "deleting the output_dir directory:--- "+ arcgis_tmpDir+"\\output_dir"
                #shutil.rmtree(arcgis_tmpDir+"\\output_dir") 
            except OSError as oserror:
                print "error : %s", oserror 
        if os.path.isdir(arcgis_tmpDir): 
            try:
                print "deleting the temporary directory:--- "+ arcgis_tmpDir
                #shutil.rmtree(arcgis_tmpDir) 
            except OSError as oserror:
                print "error : %s", oserror         
    
if __name__ == "__main__":
    main()
