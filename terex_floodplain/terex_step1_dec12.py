import arcpy
import math
import numpy
import os
import time
import uuid
#---------------------------------------------------------------------------------
# This uses the TerEX script available from the website
# http://cnr.usu.edu/htm/facstaff/belmont-hydrology-and-fine-sediment-lab/belmont-lab-resources
# For more information:
# Stout, J. and Belmont. P. (2013) TerEx Toolbox for semi-automated selection of terrace and floodplain features from lidar.
# Earth Surface Processes and Landforms, in press
#----------------------------------------------------------------------------------
def terex_process(workspace,dem,elev,minarea,single_stream,width,csize,react,sf):
    #Input Files and information, set workspace, check out Spatial Analyst Extension
    arcpy.env.workspace = workspace
    dem_in = dem
    
    elev_delta = elev
    min_area = minarea
    stream_in = single_stream
    width = width
    cell_size = csize
    rectangle = react
    smooth = sf
    arcpy.env.overwriteOutput = True
    arcpy.CheckOutExtension('Spatial')
    try:
        arcpy.AddMessage("Started Processing...")
        arcpy.AddMessage("Unsplitting streams...")
        #Unsplit stream layer
        arcpy.UnsplitLine_management (stream_in, "stream_unsplit.shp")
        stream = "stream_unsplit.shp"


        #Execute FocalStatistics(Using Rectangle)
        relief_m = 'relief_map'
        neighborhood = arcpy.sa.NbrRectangle ( rectangle, rectangle, "CELL")
        outFocalStatistics = arcpy.sa.FocalStatistics( dem_in, neighborhood, 'RANGE', '')
        outFocalStatistics.save(relief_m)

        #Selection of Terraces
        arcpy.AddMessage("Selecting flat surfaces...")
        whereclause =  " VALUE <= " + str(elev_delta)
        print whereclause
        terrace = 'terraces'
        conditional = arcpy.sa.Con( relief_m, 1, '', whereclause)
        conditional.save(terrace)  

        #Convert Raster to Polygon
        terrace_p = 'terr_poly.shp'
        arcpy.RasterToPolygon_conversion(terrace, 'terr_poly', 'SIMPLIFY', 'VALUE')

        # Eliminate polygon parts which are too small
        arcpy.EliminatePolygonPart_management (terrace_p, "polyelim.shp", "PERCENT", 75)


        #Add area field, and Calculate areas
        terrace_poly = "terrace_poly.shp"
        arcpy.CalculateAreas_stats ("polyelim.shp", terrace_poly) 


        #Select and delete areas less than area of interest
        un = arcpy.CreateUniqueName("xxx.shp")

        whereclause2 =  "F_Area >= "  + str(min_area) 
        print whereclause2
        arcpy.MakeFeatureLayer_management (terrace_poly, "lyr")
        arcpy.SelectLayerByAttribute_management ("lyr", "NEW_SELECTION", whereclause2)
        arcpy.CopyFeatures_management ("lyr", un)


        un2 = arcpy.CreateUniqueName("xxxx.shp")
        #Select terrace within valley width of stream 
        valleywidth = float(width) / 2
        print "valley width: "+ str(valleywidth)
        arcpy.MakeFeatureLayer_management (stream, un2)
        arcpy.MakeFeatureLayer_management (un, "lyr2")
        arcpy.SelectLayerByLocation_management ("lyr2", "WITHIN_A_DISTANCE", un2, valleywidth, "NEW_SELECTION")
        arcpy.SelectLayerByLocation_management ("lyr2", "INTERSECT", un2, '', "REMOVE_FROM_SELECTION")
        arcpy.CopyFeatures_management ("lyr2", "terrace_selection.shp")  
        arcpy.DeleteFeatures_management("lyr2")
        arcpy.DeleteFeatures_management("lyr")
        #arcpy.Delete_management(terrace_poly,"")

        #Aggregate and smooth THE TERRACES
        hole = float(width)
        arcpy.cartography.AggregatePolygons("terrace_selection.shp", "aggregated.shp", cell_size, min_area, hole, "NON_ORTHOGONAL")   
        #smooth_shp_name = "smoothed_"+str(uuid.uuid1())+".shp"
        t = time.gmtime()
        smooth_shp_name = "smoothed_"+str(t.tm_year)+str(t.tm_mon)+str(t.tm_mday)+ str(t.tm_hour)+str(t.tm_min)+str(t.tm_sec)
        print "smooth name ="+ smooth_shp_name
        #arcpy.cartography.SmoothPolygon( "aggregated.shp", "smoothed.shp", "PAEK", smooth, '', "NO_CHECK")
        arcpy.cartography.SmoothPolygon( "aggregated.shp", smooth_shp_name, "PAEK", smooth, '', "NO_CHECK")

          
        arcpy.Delete_management(un)
        arcpy.Delete_management(un2)
        arcpy.Delete_management(terrace_poly,"")
        
        print "Processing Done."
    except:
        print arcpy.GetMessages()

#   C:\Users\jstout\Documents\TerEx_Website_data\tutuorial_data dem_in 0.75 5000 stream.shp 1500 3 5 45