## ---------------------------------------------------------------------------
# Tree Delineation extractor
# Description: Identifies trees 
# ---------------------------------------------------------------------------

from config import *
import pyclowder.extractors as extractors

import os,os.path
import subprocess
import shutil
import sys
import requests
import logging
import json
import tempfile
import time
import csv
import math 
from multiprocessing import Process


# Import arcpy module
from arcpy.sa import *
import arcpy


# Check out any necessary licenses
arcpy.CheckOutExtension("spatial")

global channel

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL 
    global logger   
    global channel
    channel = None
    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger('Landsatsat7mosaic')
    logger.setLevel(logging.DEBUG)
    
   #set up
    extractors.setup(extractorName=extractorName, messageType=messageType, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL, sslVerify=sslVerify)
    
    #registration
    extractors.register_extractor(registrationEndpoints)
    
    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=process_file, 
        rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

#-------------------------------------------------------------------------
def zipshp(dir_name,outname):
    subprocess.check_call(['7z', 'a','-tzip', dir_name+"\\"+outname+".zip", dir_name+"\\"+'*'], shell=False)
   
#-------------------------------------------------------------------------        
# create empty dataset in DTS
def create_empty_dataset(dataset_description, host, key):
    global sslVerify
    
    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"

    url = host+'api/datasets/createempty?key=' + key

    r = requests.post(url, headers=headers, data=json.dumps(dataset_description), verify=sslVerify)
    r.raise_for_status()
    datasetid = r.json()['id']
    print 'created an empty dataset : '+ str(datasetid)
    return datasetid
#------------------------------------------------------------------------
# upload file to a dataset in DTS
def upload_file_to_dataset(filepath, datasetid, host, key):
    uploadedfileid = None
    if(not host.endswith("/")):
        host = host+"/"    
    print datasetid 
    url = host+'api/uploadToDataset/'+datasetid+'?key=' + key
    print "url:"+url
    
    f = open(filepath,'rb')
    r = requests.post(url, files={"File":f}, verify=sslVerify)
    r.raise_for_status()
    uploadedfileid = r.json()['id']
    return uploadedfileid   
    
#--------------------------------------------------------------------------------- 
# Upload dataset metadata to DTS
def upload_derived_dataset_metadata_jsonld(datasetid, mdata, host, key):

    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"
    url = host+'api/datasets/'+ datasetid +'/metadata.jsonld?key=' + key
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()

#------------------------------------------------------------------------------------
# Upload file metadata to DTS
def upload_derived_file_metadata_jsonld(fileid, mdata, host, key):
    global sslVerify

    headers={'Content-Type': 'application/json'}

    url=host+'api/files/'+ fileid +'/metadata.jsonld?key=' + key
    
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()

#----------------------------------------------------------------------------------------------------------
def status_update(status, fileid,reply_to,correlation_id):
    """Send notification on message bus with update"""
    global extractorName
    # print status
   
    #logger.debug("[%s] : %s", fileid, status)
    global channel
    print "Status:update Channel: "+ str(channel)
    statusreport = {}
    statusreport['file_id'] = fileid
    statusreport['extractor_id'] = extractorName
    statusreport['status'] = status
    statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
    channel.basic_publish(exchange='',
                          routing_key=reply_to,
                          properties=pika.BasicProperties(correlation_id = correlation_id),
                          body=json.dumps(statusreport))   

def process_file(parameters):
    global channel 
    
    count_polygon = -1
    tmpDir = ""
    filename = ""
    inputfile = parameters['inputfile']
    fileid = parameters['fileid']
    host = parameters['host'] 
    channel = parameters['channel']
    header = parameters['header']
    key = parameters['secretKey']
    print "header: " + str(header)
    correlation_id = header.correlation_id
    reply_to = header.reply_to
    print "correlation id " + correlation_id
    print "Reply To: "+ reply_to
    print "Channel: "+ str(channel)
    if(not host.endswith("/")):
        host = host+"/"
    try:
        if inputfile is not None and os.path.isfile(inputfile): 
            #creating temporary workspace
            tmpDir = tempfile.mkdtemp()
            print "tmpDir : "+ tmpDir
                                 
            processed_dir = tmpDir + "\\processed"
            output_dir = tmpDir + "\\output"
            os.makedirs(processed_dir)
            os.makedirs(output_dir)
            
            #tree_delin(inputfile,processed_dir,output_dir,fileid,reply_to,correlation_id)
            p = Process(target=tree_delin, args=(inputfile,processed_dir,output_dir,fileid, reply_to,correlation_id,))
            p.start()
            p.join()

            # Count number of polygons
            with open(output_dir + "\\tree.csv", "r") as csv_file:
                csv_data = list(csv.reader(csv_file, delimiter=","))
                count_polygon = len(csv_data)
                      
            dataset_des={}
            dataset_des['name']= 'TreeDelineation'
            dataset_des['description'] = 'created by ArcGIS using input file with id '+ fileid +' '+ time.strftime('%Y-%m-%dT%H:%M:%S')
            datasetid = create_empty_dataset(dataset_des,host,key)
            print "datasetid: "+ datasetid
            print "output_dir: "+ output_dir+"\\tree.csv"
            
            zipshp(output_dir,"treeDelin")
            
            #new_fid = upload_file_to_dataset(output_dir+"\\tree.csv",datasetid,host,key)
            new_fid = upload_file_to_dataset(output_dir+"\\treeDelin.zip",datasetid,host,key)
            time.sleep(30)
            print "uploaded file to dataset: new file id:" + new_fid
            generated_file_url = extractors.get_file_URL(fileid=new_fid, parameters=parameters).replace("\\", "/")
            print "generated file url : " + generated_file_url               
            # Context url
            context_url = 'https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld'

            # Store results as metadata
            metadata = \
           {
                '@context': [
                    context_url, {
                        'generated_file': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_file',
                        'generated_dataset': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_dataset',
                        'polygon_count': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#polygon_count'
                    }
                ],
                'attachedTo': {
                'resourceType': 'file', 'id': parameters["fileid"]
                },
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                },
                'content': {
                    'generated_file': generated_file_url,
                    'generated_dataset': host + 'datasets/'+datasetid,
                    'polygon_count': str(count_polygon)
                }
            }

            #add metadata to original input file
            extractors.upload_file_metadata_jsonld(mdata=metadata, parameters=parameters)

            #build metadata for derived dataset
            derived_dataset_metadata = \
            {
                '@context': [
                    context_url, {
                        'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                    }
                ],
                'attachedTo': {
                    'resourceType': 'dataset', 'id': datasetid
                },
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                },
                'content': {
                    'generated_from': extractors.get_file_URL(fileid, parameters).replace("\\", "/")
                }
            }
        
            #build metadata for derived file
            derived_file_metadata = \
            {
                '@context': [
                    context_url, {
                        'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                    }
                ],
                'attachedTo': {
                    'resourceType': 'file', 'id': new_fid
                },
                'agent': {
                    '@type': 'cat:extractor',
                    'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                },
                'content': {
                    'generated_from': extractors.get_file_URL(fileid, parameters).replace("\\", "/")
                }
            }
                           
            #add metadata to derived dataset        
            upload_derived_dataset_metadata_jsonld(datasetid, derived_dataset_metadata, host, key)
        
            #add metadata to derived file        
            upload_derived_file_metadata_jsonld(new_fid, derived_file_metadata, host, key)
        else:
            print "Not a file"
    finally:
        print "Finally block "
        if os.path.isdir(processed_dir): 
            try:
                print "deleting the processed dataset directory:--- "+ tmpDir+"\\processed"
                shutil.rmtree(processed_dir)
                
            except:
                pass 
               
        if os.path.isdir(output_dir): 
           try:
               print "deleting the output_dir directory:--- "+ tmpDir+"\\output_dir"
               shutil.rmtree(output_dir) 
           except OSError as oserror:
               print "error : %s", oserror 
        if os.path.isdir(tmpDir): 
           try:
               print "deleting the temporary directory:--- "+ tmpDir
               shutil.rmtree(tmpDir) 
           except OSError as oserror:
               print "error : %s", oserror         
        

#-------------------------------------------------------------------------------------
# Delineates Tree
#------------------------------------------------------------------------------------- 
def tree_delin(inputfile,processed_dir,output_dir,fileid,reply_to,correlation_id):
    global channel
    
    #Intermediate files     
    Input_true_raster = "1"
    Input_true_raster_or_constant_value = "1"
    Input_false_raster_or_constant_value = "0"
    Input_false_raster = "0"
    OSBS_02_clip_tif__2_ = processed_dir + "\\OSBS_02_clip.tif"
    OSBS_02_clip_tif = processed_dir + "\\OSBS_02_clip.tif"
    OSBS_02_clip2_tif = processed_dir + "\\OSBS_02_clip2.tif"
    OSBS_03_invert_tif = processed_dir + "\\OSBS_03_invert.tif"
    OSBS_04_filter_tif = processed_dir + "\\OSBS_04_filter.tif"
    OSBS_05_flow_tif = processed_dir + "\\OSBS_05_flow.tif"
    OSBS_06_peaks_tif = processed_dir + "\\OSBS_06_peaks.tif"
    OSBS_07_lowVeg_tif = processed_dir + "\\OSBS_07_lowVeg.tif"
    OSBS_09_maskTemp_tif = processed_dir + "\\OSBS_09_maskTemp.tif"
    OSBS_09_mask_tif = processed_dir + "\\OSBS_09_mask.tif"
    OSBS_10_sinkFill_tif = processed_dir + "\\OSBS_10_sinkFill.tif"
    OSBS_11_flowDir_tif = processed_dir + "\\OSBS_11_flowDir.tif"
    Output_drop_raster = ""
    OSBS_08_peakPoints_shp = processed_dir + "\\OSBS_08_peakPoints.shp"
    OSBS_12_watershed_tif = processed_dir + "\\OSBS_12_watershed.tif"
    OSBS_08_nullPeaks_tif = processed_dir + "\\OSBS_08_nullPeaks.tif"
    
    #output files
    OSBS_12_shape_shp = output_dir + "\\OSBS_12_shape.shp"
    tree_csv = output_dir + "\\tree.csv"
   
    # Process: Clip
    arcpy.Clip_management(inputfile, "400200 3279400 400750 3279800", OSBS_02_clip_tif, "", "-9.999000e+003", "NONE", "NO_MAINTAIN_EXTENT")
    print 'Clip management done'
    
    # Process: Set Null
    arcpy.gp.SetNull_sa(OSBS_02_clip_tif, OSBS_02_clip_tif__2_, OSBS_02_clip2_tif, "Value=-9999")
    print 'SetNull Done'
    
    # Process: Raster Calculator
    #arcpy.gp.RasterCalculator_sa("\"%OSBS_02_clip2.tif%\"*(-1.0)", OSBS_03_invert_tif)
    OSBS_02_clip2_tif_raster = Raster(OSBS_02_clip2_tif)
    OSBS_03_invert_tif_raster = OSBS_02_clip2_tif_raster*(-1.0) 
    OSBS_03_invert_tif_raster.save(OSBS_03_invert_tif)
    print 'Raster Calculation Done'
    
    # Process: Con (2)
    arcpy.gp.Con_sa(OSBS_03_invert_tif, Input_true_raster_or_constant_value, OSBS_07_lowVeg_tif, Input_false_raster_or_constant_value, "\"VALUE\" >= -10")
    print 'Conditional'
    
    # Process: Filter
    arcpy.gp.Filter_sa(OSBS_03_invert_tif, OSBS_04_filter_tif, "LOW", "DATA")
    print 'Filter'
    
    # Process: Focal Flow
    arcpy.gp.FocalFlow_sa(OSBS_04_filter_tif, OSBS_05_flow_tif, "0")
    print 'Focal Flow'
    
    # Process: Con
    arcpy.gp.Con_sa(OSBS_05_flow_tif, Input_true_raster, OSBS_06_peaks_tif, Input_false_raster, "\"Value\" = 255")
    print 'Con'
    
    # Process: Raster Calculator (2)
    #arcpy.gp.RasterCalculator_sa("SetNull(\"%OSBS_06_peaks.tif%\",\"%OSBS_03_invert.tif%\")", OSBS_09_maskTemp_tif)
    setNullFile_raster = SetNull(OSBS_06_peaks_tif,OSBS_03_invert_tif)
    setNullFile_raster.save(OSBS_09_maskTemp_tif)
    print 'Raster Calculation 2'

    # Process: Raster Calculator (4)
    #arcpy.gp.RasterCalculator_sa("SetNull(\"%OSBS_07_lowVeg.tif%\",\"%OSBS_09_maskTemp.tif%\")", OSBS_09_mask_tif)
    setNull_OSBS_raster = SetNull(OSBS_07_lowVeg_tif,OSBS_09_maskTemp_tif)
    setNull_OSBS_raster.save(OSBS_09_mask_tif)
    print 'Raster Calculation 3'
    
    # Process: Fill
    arcpy.gp.Fill_sa(OSBS_09_mask_tif, OSBS_10_sinkFill_tif, "")
    print 'Fill' 
    
    #Process: Flow Direction
    arcpy.gp.FlowDirection_sa(OSBS_10_sinkFill_tif, OSBS_11_flowDir_tif, "NORMAL", Output_drop_raster)
    print 'Flow direction'
    
    # Process: Raster Calculator (3)
    # arcpy.gp.RasterCalculator_sa("SetNull(\"%OSBS_06_peaks.tif%\",\"%OSBS_06_peaks.tif%\", \"Value = 0\")", OSBS_08_nullPeaks_tif)
    setNull_OSBS_peak_raster = SetNull(OSBS_06_peaks_tif,OSBS_06_peaks_tif, "Value = 0")
    setNull_OSBS_peak_raster.save(OSBS_08_nullPeaks_tif)

    # Process: Raster to Point
    arcpy.RasterToPoint_conversion(OSBS_08_nullPeaks_tif, OSBS_08_peakPoints_shp, "Count")

    # Process: Watershed
    arcpy.gp.Watershed_sa(OSBS_11_flowDir_tif, OSBS_08_peakPoints_shp, OSBS_12_watershed_tif, "POINTID")
    print 'Watershed'
    
    # Process: Raster to Polygon
    arcpy.RasterToPolygon_conversion(OSBS_12_watershed_tif, OSBS_12_shape_shp, "NO_SIMPLIFY", "Value")

    # Process: Add Geometry Attributes
    arcpy.AddGeometryAttributes_management(OSBS_12_shape_shp, "AREA;CENTROID", "", "", "")
    print 'Add Geometry' 
    
    # Process: Calculate Value
    ##arcpy.CalculateValue_management("math.sqrt(AREA/math.pi)", "", "Variant")
    arcpy.AddField_management(OSBS_12_shape_shp, "RADIUS", "DOUBLE")
    fields = ['POLY_AREA', 'RADIUS']
    with arcpy.da.UpdateCursor(OSBS_12_shape_shp, fields) as cursor:
        for row in cursor:
            row[1]= math.sqrt(row[0]/math.pi)
            cursor.updateRow(row)
    print 'calculate value'
    
    # Process: Export Feature Attribute to ASCII
    #arcpy.ExportXYv_stats(OSBS_12_shape_shp, "centroid_x;centroid_y;poly_area;id;radius", "COMMA", tree_csv, "ADD_FIELD_NAMES")
    export_fields = ["ID", "CENTROID_X", "CENTROID_Y", "POLY_AREA", "RADIUS"]
    arcpy.ExportXYv_stats(OSBS_12_shape_shp,export_fields, "SPACE", tree_csv, "ADD_FIELD_NAMES")
    print 'Done'

if __name__ == "__main__":
    global channel
    main()
