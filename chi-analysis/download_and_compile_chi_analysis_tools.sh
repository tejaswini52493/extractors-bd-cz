#!/usr/bin/env bash

# Setting variables
CHI_SRC_DIR="chi-analysis-source"

# Clone the Chi Analysis tools source code if it has not been already done
if [[ ! -d ${CHI_SRC_DIR} ]]; then
    git clone https://github.com/LSDtopotools/LSDTopoTools_ChiMudd2014 ${CHI_SRC_DIR}
fi

# Go to source code directory
cd ${CHI_SRC_DIR}

# Fix for Mac OS
if [[ "$OSTYPE" == "darwin"* ]]; then
    GNU_CC=$1 # Precise name of the installed GNU compiler (e.g. g++-6). This is required if running the extractor in Mac OS X, otherwise this parameter is not needed.

    if [[ ${GNU_CC} == "" ]]; then
        echo "Precise name of GNU compiler (e.g. g++-6) should be provided as the first parameter to this script. Please try again with the parameter."
        exit 0
    fi
    sed -i '.bak' 's/Array1D\<int\>/Array1D\<T\>/g' TNT/tnt_sparse_matrix_csr.h
    sed -i '.bak' 's/\bCC=g\+\+\b/CC='${GNU_CC}'/g' $(ls driver_functions_MuddChi2014/*.make)
fi

# Go to driver functions
cd driver_functions_MuddChi2014

# Compile the individual programs
make -f chi_step1_write_junctions.make
make -f chi_step2_write_channel_file.make
make -f chi_get_profiles.make
make -f chi_m_over_n_analysis.make

