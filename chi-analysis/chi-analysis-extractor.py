#!/usr/bin/env python

import logging
from config import *
import subprocess
import tempfile
import shutil
import zipfile
import sys
import xmltodict
import gdal
import time
import requests
import json
import pyclowder.api as api
import pyclowder.extractors as extractors
from calculations import calculate_minimum_movern


def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger, registrationEndpoints, mountedPaths

    gdal.UseExceptions()

    # set logging
    logging.basicConfig(format="%(levelname)-7s : %(name)s -  %(message)s", level=logging.WARN)
    logging.getLogger("pyclowder.extractors").setLevel(logging.INFO)
    logger = logging.getLogger("extractor")
    logger.setLevel(logging.DEBUG)

    # setup
    extractors.setup(extractorName=extractorName,
                     messageType=messageType,
                     rabbitmqURL=rabbitmqURL,
                     rabbitmqExchange=rabbitmqExchange)

    # register extractor info
    extractors.register_extractor(registrationEndpoints)

    # connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   processFileFunction=process_file,
                                   rabbitmqExchange=rabbitmqExchange,
                                   rabbitmqURL=rabbitmqURL)


# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global extractorName, logger
    data_directory = None
    bil_file_present = False
    hdr_file_present = False
    is_processed = False
    bil_filename_prefix = ""
    driver_file_fd = None
    zipped_file = None
    out_temp_file = None

    try:
        # Getting input file details
        inputfile = parameters["inputfile"]
        fileid = parameters['fileid']

        clowder_host = parameters['host']
        if not clowder_host.endswith("/"):
            clowder_host += "/"

        key = parameters['secretKey']

        # Create temporary directory to work with files
        data_directory = tempfile.mkdtemp()
        logger.debug("Data directory: " + data_directory)

        # Read the zip file and make sure that it contains the needed .bil and .hdr file
        zipped_file = zipfile.ZipFile(inputfile, 'r')
        zipped_contents = zipped_file.namelist()

        for filename in zipped_contents:
            file_ext = os.path.splitext(filename)[1]
            if file_ext == ".bil":
                bil_filename_prefix = os.path.splitext(filename)[0]
                bil_file_present = True
            elif file_ext == ".hdr":
                hdr_file_present = True
            elif file_ext == ".tree" or file_ext == ".chan" or file_ext == ".movern":
                is_processed = True
                break

        if not bil_file_present:
            logger.error("BIL format file is not present in the zip file")
            sys.exit(1)
        elif not hdr_file_present:
            logger.error("Header file is not present in the zip file")
            sys.exit(1)
        elif is_processed:
            logger.info("Chi Analysis already done on the contents of this zip file. Skipping!")
            sys.exit(1)

        # Extract zip file into data directory
        zipped_file.extractall(data_directory)

        # Create parameter file in data directory
        (driver_file_fd, driver_file) = tempfile.mkstemp(suffix=".driver", dir=data_directory, text=True)
        (_, driver_filename) = os.path.split(driver_file)

        # Populating parameter file with contents from chi_parameters.xml file
        populate_driver_file(driver_file_fd, bil_filename_prefix)

        # Run the program chi1_write_junctions.exe
        write_junctions_status = subprocess.call(
            ["./chi-analysis-source/driver_functions_MuddChi2014/chi1_write_junctions.exe",
             data_directory, driver_filename], stdout=subprocess.PIPE)

        # If error, then stop running the extractor
        if write_junctions_status != 0:
            logger.error("Writing junctions failed")
            sys.exit(1)
        else:
            logger.info("Writing junctions succeeded")

        # Get the junction number for performing Chi Analysis
        junction_number = get_junction_number(os.path.join(data_directory, bil_filename_prefix + '_JI' + '.bil'))
        logger.debug("Junction number is " + str(junction_number))

        # Replace junction number in driver file
        replace_junction_number(os.path.join(data_directory, driver_file), junction_number)

        # Run the program chi2_write_channel_file.exe
        write_channel_status = subprocess.call(
            ["./chi-analysis-source/driver_functions_MuddChi2014/chi2_write_channel_file.exe",
             data_directory, driver_filename], stdout=subprocess.PIPE)

        # If error, then stop running the extractor
        if write_channel_status != 0:
            logger.error("Writing channel failed")
            sys.exit(1)
        else:
            logger.info("Writing channel succeeded")

        # Run the program chi2_write_channel_file.exe
        m_over_n_status = subprocess.call(
            ["./chi-analysis-source/driver_functions_MuddChi2014/chi_m_over_n_analysis.exe",
             data_directory, driver_filename], stdout=subprocess.PIPE)

        # If error, then stop running the extractor
        if m_over_n_status != 0:
            logger.error("m over n analysis failed")
            sys.exit(1)
        else:
            logger.info("m over n analysis succeeded")

        # Run the program chi_get_profiles.exe
        chi_profiles_status = subprocess.call(
            ["./chi-analysis-source/driver_functions_MuddChi2014/chi_get_profiles.exe",
             data_directory, driver_filename], stdout=subprocess.PIPE)

        # If error, then stop running the extractor
        if chi_profiles_status != 0:
            logger.error("Extracting chi profiles failed")
            sys.exit(1)
        else:
            logger.info("Extracting chi profiles succeeded")

        # Find .movern filename
        m_over_n_filename = None
        for temp_filename in os.listdir(data_directory):
            if temp_filename.endswith(".movern"):
                m_over_n_filename = temp_filename

        # Calculate minimum m over n value
        min_m_over_n = calculate_minimum_movern(data_directory + "/", m_over_n_filename)
        logger.info("Calculated m over n ratio at minimum AICc value and generated plots of collinearity test")

        # Create dataset
        dataset_name = "Chi Analysis"
        dataset_description = "Created by the Chi Analysis extractor using input file with id " + fileid + " at " + \
                              time.strftime('%Y-%m-%dT%H:%M:%S')
        datasetid = api.create_dataset(clowder_host, dataset_name, dataset_description, secret_key=key)

        # Create zip file
        out_temp_file = tempfile.mktemp(".zip", prefix="Chi_Analysis")
        out_zipped_file = zipfile.ZipFile(out_temp_file, 'w')

        # Add generated files to the zip file
        for root, dirs, files in os.walk(data_directory):
            for out_file in files:
                out_file_full_path = os.path.join(data_directory, out_file)
                out_zipped_file.write(out_file_full_path, os.path.basename(out_file_full_path))

        # Close output zip file
        out_zipped_file.close()

        # Upload the zip file into the dataset
        generated_fileid = api.upload_files_to_dataset(clowder_host, datasetid, [out_temp_file], secret_key=key)
        generated_file_url = extractors.get_file_URL(generated_fileid, parameters)
        generated_dataset_url = clowder_host + "api/datasets/" + datasetid
        input_file_url = extractors.get_file_URL(fileid, parameters)

        logger.info("Generated dataset URL: " + generated_dataset_url)
        logger.info("Generated file URL: " + generated_file_url)

        # Context URL
        context_url = "https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld"

        # Create JSON-LD metadata for input file
        file_metadata = \
            {
                "@context": [
                    context_url, {
                        "generated_file": "http://clowder.ncsa.illinois.edu/" + extractorName + "#generated_file",
                        "generated_dataset": "http://clowder.ncsa.illinois.edu/" + extractorName + "#generated_dataset",
                        "m_over_n_ratio_at_minimum_AICc_value": "http://clowder.ncsa.illinois.edu/" + extractorName +
                                                                "#m_over_n_ratio_at_minimum_AICc_value"
                    }
                ],
                "attachedTo": {
                    "resourceType": "file", "id": fileid
                },
                "agent": {
                    "@type": "cat:extractor",
                    "extractor_id": "https://clowder.ncsa.illinois.edu/clowder/api/extractors/" + extractorName
                },
                "content": {
                    "generated_file": generated_file_url,
                    "generated_dataset": clowder_host + "datasets/" + datasetid,
                    "m_over_n_ratio_at_minimum_AICc_value": str(min_m_over_n)
                }
            }

        # Add metadata to the input file
        extractors.upload_file_metadata_jsonld(mdata=file_metadata, parameters=parameters)

        # Create JSON-LD metadata for derived dataset
        derived_dataset_metadata = \
            {
                "@context": [
                    context_url, {
                        "generated_from": "http://clowder.ncsa.illinois.edu/" + extractorName + "#generated_from",
                        "m_over_n_ratio_at_minimum_AICc_value": "http://clowder.ncsa.illinois.edu/" + extractorName +
                                                                "#m_over_n_ratio_at_minimum_AICc_value"
                    }
                ],
                "attachedTo": {
                    "resourceType": "dataset", "id": datasetid
                },
                "agent": {
                    "@type": "cat:extractor",
                    "extractor_id": "https://clowder.ncsa.illinois.edu/clowder/api/extractors/" + extractorName
                },
                "content": {
                    "generated_from": input_file_url,
                    "m_over_n_ratio_at_minimum_AICc_value": str(min_m_over_n)
                }
            }

        # Add metadata to the derived dataset
        upload_derived_dataset_metadata_jsonld(datasetid, derived_dataset_metadata, clowder_host, key)

        # Add metadata to the derived file
        upload_derived_file_metadata_jsonld(generated_fileid, derived_dataset_metadata, clowder_host, key)

    except IOError, i:
        logger.error("Error: " + i.strerror)

    except OSError, o:
        logger.error("Error: " + o.strerror)

    except RuntimeError, e:
        logger.error("Error: " + e.message)

    finally:
        # Close driver file
        if driver_file_fd is not None and os.fstat(driver_file_fd) is not None:
            os.close(driver_file_fd)

        # Close input zip file
        if zipped_file is not None:
            zipped_file.close()

        # Delete output zip file
        if out_temp_file is not None and os.path.isfile(out_temp_file):
            os.remove(out_temp_file)

        # Delete data directory
        if data_directory is not None and os.path.exists(data_directory):
            shutil.rmtree(data_directory)


# This method generates the driver file by reading the chi_parameters.xml
def populate_driver_file(driver_file_fd, bil_filename_prefix):

    # Add BIL file name (without extension) as the first entry in the driver file
    os.write(driver_file_fd, bil_filename_prefix)
    os.write(driver_file_fd, "\n")

    # Parse chi_parameters.xml file
    chi_parameters = xmltodict.parse(file("chi_parameters.xml"))
    for item in chi_parameters["parameters"]:
        os.write(driver_file_fd, chi_parameters["parameters"][item])
        os.write(driver_file_fd, "\n")


# This method calculates and returns the junction number to be used in Chi Analysis
def get_junction_number(junction_index_file):

    dataset = gdal.Open(junction_index_file)
    band = dataset.GetRasterBand(1)
    cols = band.XSize
    rows = band.YSize
    no_data = band.GetNoDataValue()

    # Read the raster value as array and reshape it into a 1-D array for manipulation purposes
    raster_value = band.ReadAsArray(0, 0, cols, rows)
    raster_value_list = raster_value.reshape(cols * rows)

    # Cleanup the list by removing the NODATA value
    raster_value_list_cleaned = [value for value in raster_value_list if value != no_data]

    # Sort cleaned up value list
    raster_value_list_cleaned_sorted = sorted(raster_value_list_cleaned)

    # Close dataset
    dataset = None

    # Return the third smallest junction index value
    return int(raster_value_list_cleaned_sorted[2])


# This method replaces the default junction number (-1) in the driver file with the junction number in the parameter
def replace_junction_number(driver_filename, junction_number):
    with open(driver_filename, 'r+') as df:
        data = df.read()
        df.seek(0)
        df.truncate()
        df.write(data.replace("-1", str(junction_number)))


# ---------------------------------------------------------------------------------
# Upload dataset metadata to DTS
def upload_derived_dataset_metadata_jsonld(datasetid, mdata, host, key):

    headers = {"Content-Type": "application/json"}

    url = host + "api/datasets/" + datasetid + "/metadata.jsonld?key=" + key
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()


# ------------------------------------------------------------------------------------
# Upload file metadata to DTS
def upload_derived_file_metadata_jsonld(fileid, mdata, host, key):

    headers = {"Content-Type": "application/json"}
    url = host + "api/files/" + fileid + "/metadata.jsonld?key=" + key

    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()


if __name__ == '__main__':
    main()
