# This method was created using the source code of
# https://github.com/LSDtopotools/LSDPlotting/blob/master/LSDChiPlotting/AICc_plotting.py, which was originally written
# by Simon M. Mudd (https://github.com/simon-m-mudd)

# This method returns the m / n value at minimum AICc value. This value is used in choosing the appropriate .tree file
# when visualizing Chi Analyses results. This method also creates plots that allow inspection of the best fit AICc
# values and their standard deviations for the collinearity test as well as for individual channels. The plots are saved
# in the data directory.

import numpy as np
import matplotlib
matplotlib.use('Agg')  # Do this before importing pylab or pyplot to turn off using display
import matplotlib.pyplot as plt
from matplotlib import rcParams


def calculate_minimum_movern(data_directory, filename):
    label_size = 20
    axis_size = 28

    # Set up fonts for plots
    rcParams['font.family'] = 'sans-serif'
    rcParams['font.sans-serif'] = ['arial']
    rcParams['font.size'] = label_size
    output_figure_format = 'png'

    f = open(data_directory + filename, 'r')  # Open file
    lines = f.readlines()  # Read data
    n_lines = len(lines)  # get the number of lines (=number of data)

    f.close()  # Close file

    # Get m/n values
    line = lines[0].strip().split(" ")

    n_movern = len(line) - 1

    movern = np.zeros(n_movern)
    collinear_AICc_mean = np.zeros(n_movern)
    collinear_AICc_stdd = np.zeros(n_movern)

    for i in range(0, n_movern):
        movern[i] = float(line[i + 1])

    # Get AICc colinearity means
    line = lines[1].strip().split(" ")

    for i in range(0, n_movern):
        collinear_AICc_mean[i] = float(line[i + 1])

        # Get AICc colinearity standard deviations
    line = lines[2].strip().split(" ")

    for i in range(0, n_movern):
        collinear_AICc_stdd[i] = float(line[i + 1])

    minimum_collinear = min(collinear_AICc_mean)

    # Get m/n value at minimum AICc value
    minimum_element = 0
    for i in range(0, n_movern):
        if collinear_AICc_mean[i] == minimum_collinear:
            minimum_element = i

    minimum_mn_collinear = movern[minimum_element]
    minimum_stdd_AIC_collinear = collinear_AICc_stdd[minimum_element]

    line_for_variability = np.zeros(n_movern)
    for i in range(0, n_movern):
        line_for_variability[i] = minimum_stdd_AIC_collinear + minimum_collinear

    # Figure out how many channels there are
    n_channels = (n_lines - 3) / 2

    AICc_mean_vecvec = np.zeros((n_channels, n_movern))
    AICc_stdd_vecvec = np.zeros((n_channels, n_movern))

    # loop through the channels
    for i in range(0, n_channels):
        line = lines[2 * i + 3].strip().split(" ")
        line2 = lines[2 * i + 4].strip().split(" ")

        for j in range(0, n_movern):
            AICc_mean_vecvec[i, j] = float(line[j + 1])
            AICc_stdd_vecvec[i, j] = float(line2[j + 1])

    # =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # AICc plot for the collinear test
    fig = plt.figure(1, facecolor='white', figsize=(10, 7.5))
    ax = fig.add_subplot(1, 1, 1)

    dmn = movern[1] - movern[0]
    plt.xlim(movern.min() - dmn * 0.5, movern.max() + dmn * 0.5)

    plt.errorbar(movern, collinear_AICc_mean, collinear_AICc_stdd, color='blue', lw=3)
    plt.plot(movern, line_for_variability, 'k', lw=3, dashes=(10, 2), alpha=0.5)
    plt.fill_between(movern, collinear_AICc_mean + collinear_AICc_stdd, collinear_AICc_mean - collinear_AICc_stdd,
                     color='grey', alpha=0.3, lw=3)

    plt.annotate('Minimum $AICc$ at $m/n$ = ' + str(minimum_mn_collinear),
                 xy=(minimum_mn_collinear, minimum_collinear + minimum_stdd_AIC_collinear), xycoords='data',
                 xytext=(0.8, 0.90), textcoords='axes fraction',
                 arrowprops=dict(arrowstyle="fancy",
                                 color="0.5",
                                 shrinkB=5,
                                 connectionstyle="arc3,rad=0.1",
                                 ),
                 horizontalalignment='right', verticalalignment='top', fontsize=label_size)

    # Configure final plot
    ax.spines['top'].set_linewidth(2.5)
    ax.spines['left'].set_linewidth(2.5)
    ax.spines['right'].set_linewidth(2.5)
    ax.spines['bottom'].set_linewidth(2.5)
    ax.tick_params(axis='both', width=2.5)

    plt.xlabel('$m/n$', fontsize=axis_size)
    plt.ylabel('$AICc$', fontsize=axis_size)
    plt.title('Results of the collinearity test', fontsize=label_size)
    plt.savefig(data_directory + "AICc_plot_collinearity_test" + "." + output_figure_format, format=output_figure_format)

    # =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    # AICc plot for the individual channels
    for i in range(0, n_channels):
        thisfig = plt.figure(i + 2, facecolor='white', figsize=(10, 7.5))
        ax = thisfig.add_subplot(1, 1, 1)

        plt.errorbar(movern, AICc_mean_vecvec[i], AICc_stdd_vecvec[i], lw=3, color='blue')

        plt.xlim(movern.min() - dmn * 0.5, movern.max() + dmn * 0.5)

        thischanAICc = AICc_mean_vecvec[i]
        minimum_chan = min(AICc_mean_vecvec[i])

        minimum_element = 0
        for j in range(0, n_movern):
            if AICc_mean_vecvec[i, j] == minimum_chan:
                minimum_element = j

        minimum_mn_chan = movern[minimum_element]
        minimum_stdd_AIC_chan = AICc_stdd_vecvec[i, minimum_element]

        for j in range(0, n_movern):
            line_for_variability[j] = minimum_stdd_AIC_chan + minimum_chan

        plt.plot(movern, line_for_variability, 'k', lw=3, dashes=(10, 2), alpha=0.5)
        plt.fill_between(movern, AICc_mean_vecvec[i] + AICc_stdd_vecvec[i], AICc_mean_vecvec[i] - AICc_stdd_vecvec[i],
                         lw=3, color='grey', alpha=0.3)

        plt.annotate('Minimum $AICc$ at $m/n$ = ' + str(minimum_mn_chan),
                     xy=(minimum_mn_chan, minimum_chan + minimum_stdd_AIC_chan), xycoords='data',
                     xytext=(0.8, 0.90), textcoords='axes fraction',
                     arrowprops=dict(arrowstyle="fancy",  # linestyle="dashed",
                                     color="0.5",
                                     shrinkB=5,
                                     connectionstyle="arc3,rad=0.1",
                                     ),
                     horizontalalignment='right', verticalalignment='top', fontsize=label_size)

        # Configure final plot
        plt.xlabel('$m/n$', fontsize=axis_size)
        plt.ylabel('$AICc$', fontsize=axis_size)
        plt.title('Results for channel number ' + str(i), fontsize=label_size)

        ax.spines['top'].set_linewidth(3)
        ax.spines['left'].set_linewidth(3)
        ax.spines['right'].set_linewidth(3)
        ax.spines['bottom'].set_linewidth(3)
        ax.tick_params(axis='both', width=3)
        plt.savefig(data_directory + "AICc_plot_channel_" + str(i) + "." + output_figure_format,
                    format=output_figure_format)

    return minimum_mn_collinear
