## ---------------------------------------------------------------------------
# Landsat 7 extractor
# Description: Mosiac given landsat 7 images and computes NDVI
# ---------------------------------------------------------------------------

from config import *
#import pymedici.extractors as extractors
import pyclowder.extractors as extractors

import os,os.path
import subprocess
import shutil
import sys
import requests
import logging
import re
import json
import tempfile
import time
import xmltodict
import csv
from multiprocessing import Process, Array


# Import arcpy module
from arcpy.sa import *
import arcpy


# Check out any necessary licenses
arcpy.CheckOutExtension("spatial")

global channel

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL 
    global logger   
    global channel
    channel = None
    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger('Landsatsat7mosaic')
    logger.setLevel(logging.DEBUG)
    
   #set up
    extractors.setup(extractorName=extractorName, messageType=messageType, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL, sslVerify=sslVerify)
    
    #registration
    extractors.register_extractor(registrationEndpoints)
    
    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=process_file, 
        rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

#-------------------------------------------------------------------------        
# create empty dataset in DTS
def create_empty_dataset(dataset_description, host, key):
    global sslVerify
    
    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"

    url = host+'api/datasets/createempty?key=' + key

    r = requests.post(url, headers=headers, data=json.dumps(dataset_description), verify=sslVerify)
    r.raise_for_status()
    datasetid = r.json()['id']
    print 'created an empty dataset : '+ str(datasetid)
    return datasetid
#------------------------------------------------------------------------
# upload file to a dataset in DTS
def upload_file_to_dataset(filepath, datasetid, host, key):
    uploadedfileid = None
    if(not host.endswith("/")):
        host = host+"/"    
    print datasetid 
    url = host+'api/uploadToDataset/'+datasetid+'?key=' + key
    print "url:"+url
    
    f = open(filepath,'rb')
    r = requests.post(url, files={"File":f}, verify=sslVerify)
    r.raise_for_status()
    uploadedfileid = r.json()['id']
    return uploadedfileid   
    
#--------------------------------------------------------------------------------- 
# Upload dataset metadata to DTS
def upload_derived_dataset_metadata_jsonld(datasetid, mdata, host, key):

    headers = {'Content-Type': 'application/json'}
    if(not host.endswith("/")):
        host = host+"/"
    url = host+'api/datasets/'+ datasetid +'/metadata.jsonld?key=' + key
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()

#------------------------------------------------------------------------------------
# Upload file metadata to DTS
def upload_derived_file_metadata_jsonld(fileid, mdata, host, key):
    global sslVerify

    headers={'Content-Type': 'application/json'}

    url=host+'api/files/'+ fileid +'/metadata.jsonld?key=' + key
    
    r = requests.post(url, headers=headers, data=json.dumps(mdata), verify=sslVerify)
    r.raise_for_status()

#----------------------------------------------------------------------------------------------------------
def status_update(status, fileid,reply_to,correlation_id):
    """Send notification on message bus with update"""
    global extractorName
    # print status
   
    #logger.debug("[%s] : %s", fileid, status)
    global channel
    print "Status:update Channel: "+ str(channel)
    statusreport = {}
    statusreport['file_id'] = fileid
    statusreport['extractor_id'] = extractorName
    statusreport['status'] = status
    statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
    channel.basic_publish(exchange='',
                          routing_key=reply_to,
                          properties=pika.BasicProperties(correlation_id = correlation_id),
                          body=json.dumps(statusreport))   
def process_file(parameters):
    
    global channel 
    tmpDir = ""
    filename = ""
    inputfile = parameters['inputfile']
    fileid = parameters['fileid']
    host = parameters['host'] 
    channel = parameters['channel']
    header = parameters['header']
    key = parameters['secretKey']
    print "header: " + str(header)
    correlation_id = header.correlation_id
    reply_to = header.reply_to
    print "correlation id " + correlation_id
    print "Reply To: "+ reply_to
    print "Channel: "+ str(channel)
    if(not host.endswith("/")):
        host = host+"/"
    try:
        if inputfile is not None and os.path.isfile(inputfile): 
            #creating temporary workspace
            tmpDir = tempfile.mkdtemp()
            print "tmpDir : "+ tmpDir
            (tmpDir, shp_dir, csv_file_name) = unzip(tmpDir,inputfile)
            if(csv_file_name != ""):
                processed_dir = tmpDir + "\\processed"
                os.makedirs(processed_dir)

                # Shared array to store mean, standard deviation, minimum, and maximum raster cell values of output
                # geotiff
                stats_array = Array('f', range(4))
            
                p = Process(target=compute_ndvi, args=(tmpDir,processed_dir,fileid, reply_to,correlation_id,stats_array,))
                p.start()
                p.join()
                #compute_ndvi(tmpDir, processed_dir,fileid, reply_to,correlation_id)    
                #ziptif("001010-revised", processed_dir,"ndvi_finaloutput")
           
                dataset_des={}
                dataset_des['name']= 'Landsat7Mosaic-3'
                dataset_des['description'] = 'created by ArcGIS using input file with id '+ fileid +' '+ time.strftime('%Y-%m-%dT%H:%M:%S')
                datasetid = create_empty_dataset(dataset_des,host,key)
                print "datasetid: "+ datasetid
                #dataset_url = host + 'datasets/'+datasetid
                ##generated_dataset.append(dataset_url)
                new_fid = upload_file_to_dataset(processed_dir+"\\001010-revised.tif",datasetid,host,key)
                time.sleep(30)
                generated_file_url = extractors.get_file_URL(fileid=new_fid, parameters=parameters).replace("\\", "/")
                           
                # Context url
                context_url = 'https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld'

                # Store results as metadata
                metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_file': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_file',
                            'generated_dataset': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_dataset',
                            'stats': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#stats',
                            'mean': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#mean',
                            'std': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#std',
                            'min': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#min',
                            'max': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#max'
                        }
                    ],
                    'attachedTo': {
                    'resourceType': 'file', 'id': parameters["fileid"]
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_file': generated_file_url,
                        'generated_dataset': host + 'datasets/'+datasetid,
                        'stats': {
                            'mean': "%.4f" % stats_array[0],
                            'std': "%.4f" % stats_array[1],
                            'min': "%.4f" % stats_array[2],
                            'max': "%.4f" % stats_array[3]
                        }
                    }
                }

                #add metadata to original input file
                extractors.upload_file_metadata_jsonld(mdata=metadata, parameters=parameters)
    
                #build metadata for derived dataset
                derived_dataset_metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                        }
                    ],
                    'attachedTo': {
                        'resourceType': 'dataset', 'id': datasetid
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_from': extractors.get_file_URL(fileid, parameters).replace("\\", "/")
                    }
                }
        
                #build metadata for derived file
                derived_file_metadata = \
                {
                    '@context': [
                        context_url, {
                            'generated_from': 'http://clowder.ncsa.illinois.edu/' + extractorName + '#generated_from',
                        }
                    ],
                    'attachedTo': {
                        'resourceType': 'file', 'id': new_fid
                    },
                    'agent': {
                        '@type': 'cat:extractor',
                        'extractor_id': 'https://clowder.ncsa.illinois.edu/clowder/api/extractors/' + extractorName
                    },
                    'content': {
                        'generated_from': extractors.get_file_URL(fileid, parameters).replace("\\", "/")
                    }
                }
                           
                #add metadata to derived dataset        
                upload_derived_dataset_metadata_jsonld(datasetid, derived_dataset_metadata, host, key)
        
                #add metadata to derived file        
                upload_derived_file_metadata_jsonld(new_fid, derived_file_metadata, host, key)
        else:
            print "Not a file"
    finally:
        print "Finally block "
        if os.path.isdir(tmpDir+"\\input_landsat"): 
            try:
                print "deleting the input_dataset directory:--- "+ tmpDir+"\\input_landsat"
                shutil.rmtree(arcgis_tmpDir+"\\input_landsat")
                
            except:
                pass 
               
        if os.path.isdir(tmpDir+"\\processed"): 
            try:
                print "deleting the output_dir directory:--- "+ tmpDir+"\\processed"
                shutil.rmtree(tmpDir+"\\processed") 
            except OSError as oserror:
                print "error : %s", oserror 
        if os.path.isdir(tmpDir): 
            try:
                print "deleting the temporary directory:--- "+ tmpDir
                shutil.rmtree(tmpDir) 
            except OSError as oserror:
                print "error : %s", oserror         
#zip the shapefile 
def ziptif(name,dir_name,outname):
    subprocess.check_call(['7z', 'a','-tzip', dir_name+"\\"+outname+".zip", dir_name+"\\"+name+'*'], shell=False)
    return name  

#unzip the input file that consists of a tif and a zipped shapefile
def unzip(tmpdir, inzip):
    tmpDir=tmpdir
    #os.makedirs(tmpDir)
    subprocess.check_call(['7z', 'x', '-o%s' % tmpDir, inzip], shell=False)
    
    files = [os.path.join(tmpDir,f) for f in os.listdir(tmpDir)]
    #files = [os.path.join(dirlist,f1) for f1 in os.listdir(dirlist[0])]
    shp_dir_name = ""
    csv_file_name = ""
    for f in files:
        print "realpath : " + os.path.realpath(f)
        print os.path.basename(f)
        if os.path.isdir(f):
            print "yes, it is a directory"
            shpfiles=[os.path.join(f,fi) for fi in os.listdir(f)]
            for fi in shpfiles:
                print "shpfiles " + os.path.basename(fi)
                if fi.endswith(".zip"):
                    #m = re.split('\.shp.zip',os.path.basename(f))
                    m = re.split('\.zip',os.path.basename(fi))
                    print "boundary directory: " + m[0]
                    shp_dir = os.path.realpath(f)+"\\"+m[0]
                    subprocess.check_call(['7z', 'x', '-o%s' %shp_dir, fi], shell=False)
                    shp_dir_name = m[0]
                if fi.endswith(".csv"):
                    csv_file_name = os.path.basename(fi)        
    return (tmpDir,shp_dir_name, csv_file_name)        

def compute_ndvi(tmpdir,processed_dir,fileid, reply_to,correlation_id, stats_array):
    global scene_b3, scene_b4, scene_mtl,ndvi, reclass,rad,ref,post_ref,reclass_dict,rad_dict,ref_dict, post_ref_dict, ndvi_dict 
    global channel
    #print "Inside compute_ndvi : Channel: "+ str(channel)
    
    scene_b3 = []
    scene_b4 = []
    scene_mtl = []
    ndvi = []
    reclass = []  # First Index denotes band and second index denotes the scene. In this case we have band 3 and band 4 as first index value 0 and 1 
    rad = []
    ref = []
    post_ref = []
    reclass_dict = {}   
    rad_dict = {} 
    ref_dict = {}
    post_ref_dict = {} 
    ndvi_dict ={}
    
    
    for i in range(0,2):
        reclass.append([])
        rad.append([])
        ref.append([])
        post_ref.append([]) 
        
    # Initialize all intermediate file names
    for i in range(0,2):
        for j in range(0,2):
            if i == 0:
                reclass[i].append(processed_dir+"\\reclass_B3_"+ str(j)+".tif")
                rad[i].append(processed_dir+"\\rad_B3_"+ str(j)+".tif")
                ref[i].append(processed_dir+"\\ref_B3_"+str(j)+".tif")
                post_ref[i].append(processed_dir+"\\post_ref_B3_"+str(j)+".tif")
            else:
                reclass[i].append(processed_dir+"\\reclass_B4_"+str(j)+".tif")
                rad[i].append(processed_dir+"\\rad_B4_"+str(j)+".tif")
                ref[i].append(processed_dir+"\\ref_B4_"+str(j)+".tif")
                post_ref[i].append(processed_dir+"\\post_ref_B4_"+str(j)+".tif")            
        ndvi.append(processed_dir + "\\ndvi_"+str(i)+".tif")   
    Mosaic_tif = processed_dir + "\\Mosaic.tif"
    #print "Reclass:  " + str(reclass)     
    #print tmpdir
    files = [os.path.join(tmpdir,f) for f in os.listdir(tmpdir)]
    for d in files:
        #print "for 1: compute_ndvi: realpath : " + os.path.realpath(d) 
        subdirs = [os.path.join(d,f) for f in os.listdir(d)]
        for subdir in subdirs:
            #print "for 2: compute_ndvi: realpath : " + subdir 
            if "Scene" in subdir: 
                #print "Go inside the subdir "+ subdir
                sc_bands = [os.path.join(subdir,sc) for sc in os.listdir(subdir)]
                for sc_band in sc_bands:
                    if "_B3.TIF" in sc_band:
                        scene_b3.append(sc_band)
                    if "_B4.TIF" in sc_band:
                        scene_b4.append(sc_band)
                    if "_MTL.txt" in sc_band:
                        scene_mtl.append(sc_band) 
            if "Boundary" in subdir and os.path.isdir(subdir):
                shp_dir = subdir    
                shp_file = [os.path.join(subdir,shp) for shp in os.listdir(subdir)]                
    print "S: B3: " + str(scene_b3)
    print "S: B4: " + str(scene_b4)
    print scene_mtl 
    print shp_file
    
    #output file NDVI
    v001010_revised_tif = processed_dir+"\\001010-revised.tif"
    
    #process_band_scene(1,0,scene_b4[0],reclass,rad,ref,post_ref, reclass_dict,rad_dict,ref_dict,post_ref_dict, sun_ele, sun_earth_dist)
    for sc_num in range(0,2):
        (sun_ele, day_of_year) = mtlgenericparser(sc_num)
        sun_earth_dist = sun_earth_distance(day_of_year, tmpdir)
        for band in range(0,2):
            if band == 1:
                process_band_scene(band,sc_num,scene_b4[sc_num], sun_ele, sun_earth_dist)
                #status_update("Processed band 4 scene "+str(sc_num), fileid,reply_to,correlation_id)
            else:
                process_band_scene(band,sc_num,scene_b3[sc_num], sun_ele, sun_earth_dist) 
                #status_update("Processed band 3 scene "+str(sc_num), fileid,reply_to,correlation_id)               
        ndvi_dict["ndvi_"+ str(sc_num) + "_raster"] = (post_ref_dict["post_ref_B"+ str(1) + "_"+ str(sc_num) + "_raster"] - post_ref_dict["post_ref_B"+ str(0) + "_"+ str(sc_num) + "_raster"])/(post_ref_dict["post_ref_B"+ str(1) + "_"+ str(sc_num) + "_raster"] + post_ref_dict["post_ref_B"+ str(0) + "_"+ str(sc_num) + "_raster"])
        ndvi_dict["ndvi_"+ str(sc_num) + "_raster"].save(ndvi[sc_num])
    #  Process: Mosaic To New Raster
    arcpy.MosaicToNewRaster_management(ndvi[0]+";"+ndvi[1], processed_dir, "Mosaic.tif", "", "32_BIT_FLOAT", "", "1", "LAST", "FIRST")
    #status_update("Getting Mosaic", fileid,reply_to,correlation_id)
    # Process: Extract by Mask
    arcpy.gp.ExtractByMask_sa(Mosaic_tif, shp_file[2], v001010_revised_tif)
    #status_update("Mosaic done", fileid,reply_to, correlation_id)

    # Calculate the statistics on the raster data
    arcpy.CalculateStatistics_management(v001010_revised_tif)

    # Get the geoprocessing result object for mean
    mean_result_object = arcpy.GetRasterProperties_management(v001010_revised_tif, "MEAN")
    # Get the mean value from geoprocessing result object
    mean = mean_result_object.getOutput(0)

    # Get the geoprocessing result object for standard deviation
    std_result_object = arcpy.GetRasterProperties_management(v001010_revised_tif, "STD")
    # Get the standard deviation value from geoprocessing result object
    std = std_result_object.getOutput(0)

    # Get the geoprocessing result object for minimum
    min_result_object = arcpy.GetRasterProperties_management(v001010_revised_tif, "MINIMUM")
    # Get the minimum value from geoprocessing result object
    minimum = min_result_object.getOutput(0)

    # Get the geoprocessing result object for maximum
    max_result_object = arcpy.GetRasterProperties_management(v001010_revised_tif, "MAXIMUM")
    # Get the maximum value from geoprocessing result object
    maximum = max_result_object.getOutput(0)

    # Store results in stats array
    stats_array[0] = float(mean)
    stats_array[1] = float(std)
    stats_array[2] = float(minimum)
    stats_array[3] = float(maximum)


def mtlgenericparser(scene_num):
    i=0
    L1group_flag = False
    img_attr_flag = False
    sun_elevation = 0.0
    mtl_metadata = {}
    mtl_subgroup = {}
    tmp_mdata = {}
    group_name = ''
    with open(scene_mtl[scene_num],'rU') as csvfile:
        mtlreader = csv.reader(csvfile, delimiter=' ')
        for row in mtlreader:
            #i=i+1
            #print row
            if row[0] == 'GROUP' and row[2]=='L1_METADATA_FILE':
                L1group_flag = True
                mtl_metadata['L1_METADATA_FILE'] = {}
            elif L1group_flag == True and row[2] == 'GROUP':
                mtl_subgroup [row[4]] = {}
                group_name = row[4]
            elif L1group_flag == True and row[2] == '':
                tmp_mdata[row[4]] = row[6]
            elif L1group_flag == True and row[2] == 'END_GROUP' and group_name == row[4]:
                mtl_subgroup[group_name] = tmp_mdata
                tmp_mdata = {} 
            elif row[0] == 'END_GROUP' and row[2] == 'L1_METADATA_FILE':
                L1group_flag = False
                mtl_metadata['L1_METADATA_FILE'] = mtl_subgroup
            else:
                continue  
    #print mtl_metadata['L1_METADATA_FILE']                       
    #print "Scene ID: " + mtl_metadata['L1_METADATA_FILE']['METADATA_FILE_INFO']['LANDSAT_SCENE_ID']
    #print "Sun Elevation: " + mtl_metadata['L1_METADATA_FILE']['IMAGE_ATTRIBUTES']['SUN_ELEVATION']
    id = mtl_metadata['L1_METADATA_FILE']['METADATA_FILE_INFO']['LANDSAT_SCENE_ID']
    day_of_year =  id[13:16] 
    sun_ele = mtl_metadata['L1_METADATA_FILE']['IMAGE_ATTRIBUTES']['SUN_ELEVATION']
    print "DAY of YEAR: "+ id[13:16]    
    return (sun_ele,day_of_year)

def sun_earth_distance(day_of_year, tmpdir):
    earth_sun_dist = {}
    cols = 12
    i=0
    
    filepath = tmpdir+"\\input_landsat"
    with open(filepath+"\\earth-sundist.csv",'rU') as csvfile:
        eareader = csv.reader(csvfile, delimiter=',')
        for row in eareader:
            i=i+1
            for doy in xrange(0,cols,2):
                #earth_sun_dist[row[doy]] = row[doy+1]
                #print row[doy], row[doy+1], earth_sun_dist[row[doy+1]]
                if row[doy]!='' and row[doy+1]!= '':
                    #print i,doy,row[doy], doy+1, row[doy+1]
                    earth_sun_dist[row[doy]] = row[doy+1]
            #for doy in xrange(0,cols,2):
            #    if row[doy]!='' and row[doy+1]!= '':
            #        print row[doy], earth_sun_dist[row[doy]]
    #print "earth-sun-distance: " + earth_sun_dist[day_of_year]
    return earth_sun_dist[day_of_year]
    
    
    
def process_band_scene(band,scene,scene_band_tif,sun_elevation,sun_earth_dist):
    # Process: Reclassify (3)
    print "band : "+ str(band)+"  Scene: "+ str(scene) 
    print "sun_elevation: "+ sun_elevation + "  sun-earth distance :" + sun_earth_dist 
    band_scene_raster = "B"+ str(band) + "_"+ str(scene) + "_raster"
    if band == 0:
        band_var = 1533
    else:
        band_var = 1039 
    arcpy.gp.Reclassify_sa(scene_band_tif, "Value", "0 NODATA;1 1;2 2;3 3;4 4;5 5;6 6;7 7;8 8;9 9;10 10;11 11;12 12;13 13;14 14;15 15;16 16;17 17;18 18;19 19;20 20;21 21;22 22;23 23;24 24;25 25;26 26;27 27;28 28;29 29;30 30;31 31;32 32;33 33;34 34;35 35;36 36;37 37;38 38;39 39;40 40;41 41;42 42;43 43;44 44;45 45;46 46;47 47;48 48;49 49;50 50;51 51;52 52;53 53;54 54;55 55;56 56;57 57;58 58;59 59;60 60;61 61;62 62;63 63;64 64;65 65;66 66;67 67;68 68;69 69;70 70;71 71;72 72;73 73;74 74;75 75;76 76;77 77;78 78;79 79;80 80;81 81;82 82;83 83;84 84;85 85;86 86;87 87;88 88;89 89;90 90;91 91;92 92;93 93;94 94;95 95;96 96;97 97;98 98;99 99;100 100;101 101;102 102;103 103;104 104;105 105;106 106;107 107;108 108;109 109;110 110;111 111;112 112;113 113;114 114;115 115;116 116;117 117;118 118;119 119;120 120;121 121;122 122;123 123;124 124;125 125;126 126;127 127;128 128;129 129;130 130;131 131;132 132;133 133;134 134;135 135;136 136;137 137;138 138;139 139;140 140;141 141;142 142;143 143;144 144;145 145;146 146;147 147;148 148;149 149;150 150;151 151;152 152;153 153;154 154;155 155;156 156;157 157;158 158;159 159;160 160;161 161;162 162;163 163;164 164;165 165;166 166;167 167;168 168;169 169;170 170;171 171;172 172;173 173;174 174;175 175;176 176;177 177;178 178;179 179;180 180;181 181;182 182;183 183;184 184;185 185;186 186;187 187;188 188;189 189;190 190;191 191;192 192;193 193;194 194;195 195;196 196;197 197;198 198;199 199;200 200;201 201;202 202;203 203;204 204;205 205;206 206;207 207;208 208;209 209;210 210;211 211;212 212;213 213;214 214;215 215;216 216;217 217;218 218;219 219;220 220;221 221;222 222;223 223;224 224;225 225;226 226;227 227;228 228;229 229;230 230;231 231;232 232;233 233;234 234;235 235;236 236;237 237;238 238;239 239;240 240;241 241;242 242;243 243;244 244;245 245;246 246;247 247;248 248;249 249;250 250;251 251;252 252;253 253;254 254;255 255", reclass[band][scene], "NODATA")
    # Process: Raster Calculator (8)
    reclass_dict["reclass_"+ band_scene_raster]= Raster(reclass[band][scene])
    #print reclass_dict["reclass_"+ band_scene_raster].minimum
    #print reclass_dict["reclass_"+ band_scene_raster].maximum
    #print "After Raster"
    rad_dict["rad_"+band_scene_raster] = (0.639764*reclass_dict["reclass_"+ band_scene_raster])-5.74
    rad_dict["rad_"+ band_scene_raster].save(rad[band][scene])
    
    print "Reclass Done "
    #print rad_dict["rad_"+ band_scene_raster].minimum
    #print rad_dict["rad_"+ band_scene_raster].maximum
    
    # Process: Raster Calculator (9)
    # 39.54882089 solar angle needs to be changed based on metadata
    #Square(0.99832) dist to sun needs to be changed based on metadata
    ref_dict["ref_"+ band_scene_raster] = (3.141592654 * rad_dict["rad_"+ band_scene_raster] * Square(float(sun_earth_dist))) / (band_var * Sin(float(sun_elevation) * 3.1415926 / 180.0))
    ref_dict["ref_"+ band_scene_raster].save(ref[band][scene])

    # Process: Raster Calculator (10)
    
    post_ref_dict["post_ref_"+ band_scene_raster] = Con(ref_dict["ref_"+ band_scene_raster] <0.0,0.0,ref_dict["ref_"+ band_scene_raster])
    
    post_ref_dict["post_ref_"+ band_scene_raster].save(post_ref[band][scene])
    #print "post_ref: "+ "post_ref_"+ band_scene_raster
   

if __name__ == "__main__":
    global channel
    main()
