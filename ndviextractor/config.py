# =============================================================================
#
# In order for this extractor to run according to your preferences, 
# the following parameters need to be set. 
# 
# Some parameters can be left with the default values provided here - in that 
# case it is important to verify that the default value is appropriate to 
# your system. It is especially important to verify that paths to files and 
# software applications are valid in your system.
#
# =============================================================================

# name to show in rabbitmq queue list
extractorName = "ncsa.arcgis.landsat7mosaic"

# URL to be used for connecting to rabbitmq
rabbitmqURL = "amqp://guest:guest@localhost:5672/%2f"

# name of rabbitmq exchange
rabbitmqExchange = "clowder"

# type of files to process
messageType = "*.file.multi.files-zipped.#"

# trust certificates, set this to false for self signed certificates
sslVerify = False

# Endpoints and keys for registering extractor information in CSV format.
registrationEndpoints = "http://localhost:9000/clowder/api/extractors?key=key1,http://host2:9000/api/extractors?key=key2"


